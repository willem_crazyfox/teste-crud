<?php

use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('posts')->insert([
          'title' => str_random(10),
          'content' => str_random(1000),
          'image' =>  'https://images.unsplash.com/photo-1475522508222-1a8a3fd0734c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=63be3832d79a73a64d154616b48f9e2c&auto=format&fit=crop&w=1834&q=80',
          'category_id' => 1
      ]);
    }
}

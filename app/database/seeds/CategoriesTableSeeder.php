<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('categories')->insert([
          'id'  => 1,
          'title' => str_random(10),
          'created_at' => now(),
          'updated_at' => now()
      ]);
    }
}

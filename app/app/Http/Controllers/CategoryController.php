<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Validator;

use App\Category;


class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $categories = Category::all();

        return view('category.index', ['categories' => $categories] );

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      $validation = Validator::make( $request->all(), [
        'title' => 'required|unique:categories|max:255',
      ]);

      if(!$validation->fails()) {

        $Category = new Category();
        $Category->title = $request->title;
        $Category->save();

        $result = ['error' => false, 'message' => 'Categoria salva com sucesso'];

      } else {

        $result = ['error' => true, 'message' => 'Categoria não pode salva'];

      }

      return response()->json($result);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Category::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $validation = Validator::make( $request->all(), [
        'title' => 'required|unique:categories|max:255',
      ]);

      if(!$validation->fails()) {

        $Category = Category::find($id);
        $Category->title = $request->title;
        $Category->save();

        $result = ['error' => false, 'message' => 'Categoria salva com sucesso'];

      } else {

        $result = ['error' => true, 'message' => 'Categoria não pode salva'];

      }

      return response()->json($result);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::findOrFail($id);
        $category->delete();
        return redirect('categories')->with('success','Product has been  deleted');
    }
}

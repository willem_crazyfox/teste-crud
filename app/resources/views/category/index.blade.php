@extends('layout.master')
@section('title', 'Categorias')
@section('content')
<div id="list" class="row">
	<div class="table-responsive">
		<table class="table table-striped" cellspacing="0" cellpadding="0">
			<thead>
				<tr>
					<th>Categoria</th>
					<th class="actions">Ações</th>
				</tr>
			</thead>
			<tbody>
        @foreach($categories as $category)
				<tr>
					<td>{{$category->title}}</td>
					<td class="actions">
						<a class="btn btn-success btn-xs" href="{{ URL::to('categories/'.$category->id)}}">Visualizar</a>
						<a class="btn btn-warning btn-xs" href="{{ URL::to('categories/'.$category->id.'/edit')}}">Editar</a>
          	<form action="{{action('CategoryController@destroy', $category->id)}}" method="post" style="display:inline">
							<input type="hidden" name="_method" value="DELETE">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
            	<button class="btn btn-danger btn-xs" type="submit">Excluir</button>
          	</form>
					</td>
				</tr>
        @endforeach
      </tbody>
    </table>
  </div>
</div>
@endsection

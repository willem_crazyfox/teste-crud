<html>
    @include('includes.head')
    <link href="{!! asset('css/app.css') !!}" media="all" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="{!! asset('js/app.js') !!}"></script>
    <body>
      <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav">
              <li>
                  <a href="categories">Categorias</a>
              </li>
              <li>
                  <a href="posts">Noticias</a>
              </li>
          </ul>
      </div>
    </nav>
    <div class="container-fluid">
      @yield('content')
    </div>
    </body>
</html>

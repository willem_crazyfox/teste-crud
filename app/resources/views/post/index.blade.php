@extends('layout.master')
@section('title', 'Notícias')
@section('content')
<div id="list" class="row">
	<div class="table-responsive">
		<table class="table table-striped" cellspacing="0" cellpadding="0">
			<thead>
				<tr>
					<th>Post</th>
					<th class="actions">Ações</th>
				</tr>
			</thead>
			<tbody>
        @foreach($posts as $post)
				<tr>
					<td>{{$post->title}}</td>
					<td class="actions">
						<a class="btn btn-success btn-xs" href="{{ URL::to('posts/'.$post->id)}}">Visualizar</a>
						<a class="btn btn-warning btn-xs" href="{{ URL::to('posts/'.$post->id.'/edit')}}">Editar</a>
          	<form action="{{action('PostController@destroy', $post->id)}}" method="post" style="display:inline">
							<input type="hidden" name="_method" value="DELETE">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
            	<button class="btn btn-danger btn-xs" type="submit">Excluir</button>
          	</form>
					</td>
				</tr>
        @endforeach
      </tbody>
    </table>
  </div>
</div>
@endsection
